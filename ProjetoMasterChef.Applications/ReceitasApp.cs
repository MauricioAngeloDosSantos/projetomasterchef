﻿using ProjetoMasterChef.Domains.Entities;
using ProjetoMasterChef.Domains.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoMasterChef.Applications
{
    public class ReceitasApp : AppBase<ReceitaVM>
    {
        public ReceitasApp(string httpClientAddress) : base(httpClientAddress, "receitas")
        {
        }

    }
}
