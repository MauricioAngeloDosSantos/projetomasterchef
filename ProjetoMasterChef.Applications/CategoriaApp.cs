﻿using ProjetoMasterChef.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetoMasterChef.Applications
{
    public class CategoriaApp : AppBase<Categoria>
    {
        public CategoriaApp(string httpClientAddress) : base(httpClientAddress, "categorias")
        {
            
        }
    }
}
