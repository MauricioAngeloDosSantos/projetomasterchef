﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ProjetoMasterChef.Applications.Helpers
{
    public class WebApiHelper
    {

        public static HttpClient Connect(string HttpClientAddress)
        {
            HttpClient client = new HttpClient();

            client.BaseAddress = new Uri(HttpClientAddress);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

    }



    public class WebApiHelper<TEntity> where TEntity : class
    {
        private string controller = "";

        public WebApiHelper(string controller)
        {
            this.controller = controller;
        }

        public async Task<IEnumerable<TEntity>> Get(string HttpClientAddress)
        {
            return await WebApiHelper<IEnumerable<TEntity>>.Get(HttpClientAddress, this.controller);
        }

        public async Task<TEntity> Get(string HttpClientAddress, long id)
        {
            return await WebApiHelper<TEntity>.Get(HttpClientAddress, this.controller, id);
        }


        public void Put(string HttpClientAddress, long id, TEntity entity)
        {
            WebApiHelper<TEntity>.Put(HttpClientAddress, this.controller, id, entity);
        }

        public void Post(string HttpClientAddress, TEntity entity)
        {
            WebApiHelper<TEntity>.Post(HttpClientAddress, this.controller, entity);
        }

        public void Delete(string HttpClientAddress, long id)
        {
            WebApiHelper<TEntity>.Delete(HttpClientAddress, this.controller, id);
        }

        public async static Task<TEntity> Get(string HttpClientAddress, string controller)
        {
            HttpResponseMessage response = await WebApiHelper.Connect(HttpClientAddress).GetAsync(controller);

            TEntity entity = await response.Content.ReadAsAsync<TEntity>();

            return entity;
        }

        public async static Task<TEntity> Get(string HttpClientAddress, string controller, long id)
        {
            HttpResponseMessage response = await WebApiHelper.Connect(HttpClientAddress).GetAsync(controller + "/" + id);

            var entity = await response.Content.ReadAsAsync<TEntity>();

            return entity;
        }


        public async static Task<IEnumerable<TEntity>> GetInclude(string HttpClientAddress, string controller, string tipo )
        {
            HttpResponseMessage response = await WebApiHelper.Connect(HttpClientAddress).GetAsync(controller + "?tipo=" + tipo);

            IEnumerable<TEntity> entities = await response.Content.ReadAsAsync<IEnumerable<TEntity>>();

            return entities;
        }

        public async static void Put(string HttpClientAddress, string controller, long id, TEntity entity)
        {
            HttpResponseMessage response = await WebApiHelper.Connect(HttpClientAddress).PutAsJsonAsync<TEntity>(controller + "/" + id, entity);
        }

        public async static void Post(string HttpClientAddress, string controller, TEntity entity)
        {
            HttpResponseMessage response = await WebApiHelper.Connect(HttpClientAddress).PostAsJsonAsync<TEntity>(controller, entity);
        }

        public async static void Delete(string HttpClientAddress, string controller, long id)
        {
            HttpResponseMessage response = await WebApiHelper.Connect(HttpClientAddress).DeleteAsync(controller + "/" + id);
        }
    }
}
