﻿using ProjetoMasterChef.Applications.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoMasterChef.Applications
{
    public class AppBase<TEntity> : IDisposable, IAppBase<TEntity> where TEntity : class
    {
        protected WebApiHelper<TEntity> WebApi;
        string Controller;
        string HttpClientAddress;

        public AppBase(string httpClientAddress, string controller)
        {
            this.Controller = controller;
            this.WebApi = new WebApiHelper<TEntity>(controller);
            this.HttpClientAddress = httpClientAddress;
        }

        public void Add(TEntity entity)
        {
            this.WebApi.Post(this.HttpClientAddress, entity);
        }

        public void Delete(long id)
        {
            this.WebApi.Delete(this.HttpClientAddress, id);
        }

        public void Dispose()
        {
            this.Dispose();
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await this.WebApi.Get(this.HttpClientAddress);
        }

        public async Task<TEntity> GetById(long id)
        {
            return await this.WebApi.Get(this.HttpClientAddress, id);
        }

        public void Update(long id, TEntity entity)
        {
            this.WebApi.Put(this.HttpClientAddress, id, entity);
        }
    }
}
