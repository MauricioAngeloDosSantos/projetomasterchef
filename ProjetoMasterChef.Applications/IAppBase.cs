﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoMasterChef.Applications
{
    public interface IAppBase<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void Update(long id, TEntity entity);
        void Delete(long id);
        Task<TEntity> GetById(long id);
        Task<IEnumerable<TEntity>> GetAll();
    }
}
