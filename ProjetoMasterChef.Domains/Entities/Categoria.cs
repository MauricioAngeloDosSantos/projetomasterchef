﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjetoMasterChef.Domains.Entities
{
    [Table("TbCategorias")]
    public class Categoria
    {
        [Key]
        public int IdCategoria { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "O campo '{0}', é obrigatório")]
        [MaxLength(30, ErrorMessage = "Favor informar um {0}, como no máximo {1}, caracteres")]
        public string Titulo { get; set; }

        [Display(Name = "Descrição")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "O campo '{0}', é obrigatório")]
        [MaxLength(100, ErrorMessage = "Favor informar um {0}, como no máximo {1}, caracteres")]
        public string Descricao { get; set; }

        public string Foto { get; set; }
        public string NomeDoArquivo { get; set; }
        public string ContentType { get; set; }


        public virtual IEnumerable<Receita> Receitas { get; set; }
    }
}
