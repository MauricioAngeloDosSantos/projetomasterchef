﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetoMasterChef.Domains.Entities
{
    public class MessageReturn
    {
        public enum MessageReturnType
        {
            Ok = 0,
            Warning = 1,
            Error = 2
        }

        public MessageReturn(MessageReturnType returnType, string message)
        {
            this.ReturnType = returnType;
            this.Message = message;
        }

        public MessageReturn(MessageReturnType returnType) { }

        public MessageReturnType ReturnType { get; set; }
        public string Message { get; set; }

        public static MessageReturn Erro(string message)
        {
            return new MessageReturn(MessageReturnType.Error, message);
        }

        public static MessageReturn Ok()
        {
            return new MessageReturn(MessageReturnType.Ok);
        }
    }
}
