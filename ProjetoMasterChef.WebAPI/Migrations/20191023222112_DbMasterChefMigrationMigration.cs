﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjetoMasterChef.WebAPI.Migrations
{
    public partial class DbMasterChefMigrationMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TbCategorias",
                columns: table => new
                {
                    IdCategoria = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Titulo = table.Column<string>(maxLength: 30, nullable: false),
                    Descricao = table.Column<string>(maxLength: 100, nullable: false),
                    Foto = table.Column<string>(nullable: true),
                    NomeDoArquivo = table.Column<string>(nullable: true),
                    ContentType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TbCategorias", x => x.IdCategoria);
                });

            migrationBuilder.CreateTable(
                name: "TbReceitas",
                columns: table => new
                {
                    IdReceita = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdCategoria = table.Column<int>(nullable: false),
                    Titulo = table.Column<string>(maxLength: 30, nullable: false),
                    Descricao = table.Column<string>(maxLength: 100, nullable: false),
                    Ingredientes = table.Column<string>(nullable: false),
                    ModoDePreparo = table.Column<string>(nullable: false),
                    Foto = table.Column<string>(nullable: true),
                    NomeDoArquivo = table.Column<string>(nullable: true),
                    ContentType = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TbReceitas", x => x.IdReceita);
                    table.ForeignKey(
                        name: "FK_TbReceitas_TbCategorias_IdCategoria",
                        column: x => x.IdCategoria,
                        principalTable: "TbCategorias",
                        principalColumn: "IdCategoria",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TbReceitas_IdCategoria",
                table: "TbReceitas",
                column: "IdCategoria");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TbReceitas");

            migrationBuilder.DropTable(
                name: "TbCategorias");
        }
    }
}
