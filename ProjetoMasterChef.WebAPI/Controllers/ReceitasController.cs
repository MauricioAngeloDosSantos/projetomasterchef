﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoMasterChef.Domains.Entities;
using ProjetoMasterChef.Domains.ViewModel;
using ProjetoMasterChef.WebAPI.Models;

namespace ProjetoMasterChef.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReceitasController : ControllerBase
    {
        private readonly DbMasterChefContext _context;

        public ReceitasController(DbMasterChefContext context)
        {
            _context = context;
        }

        // GET: api/Receitas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReceitaVM>>> GetReceitas()
        {
            var receitasAll = await _context.Receitas.Include(p => p.Categoria).ToListAsync();

            var receitasVM = (from info in receitasAll
                           select new ReceitaVM()
                           {
                               TituloCategoria = info.Categoria?.Titulo,
                               ContentType = info.ContentType,
                               Descricao = info.Descricao,
                               Foto = info.Foto,
                               IdCategoria = info.IdCategoria,
                               IdReceita = info.IdReceita,
                               Ingredientes = info.Ingredientes,
                               ModoDePreparo = info.ModoDePreparo,
                               NomeDoArquivo = info.NomeDoArquivo,
                               Tags = info.Tags,
                               Titulo = info.Titulo
                           }).ToList();
            
            return receitasVM;
        }

        // GET: api/Receitas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReceitaVM>> GetReceita(int id)
        {
            var receitas = await _context.Receitas.Include(p => p.Categoria).Where( p=>p.IdReceita == id).ToListAsync();

            if (receitas == null)
            {
                return NotFound();
            }

            var receita = receitas.FirstOrDefault();


            ReceitaVM receitaVM = new ReceitaVM()
            {
                TituloCategoria = receita.Categoria?.Titulo,
                ContentType = receita.ContentType,
                Descricao = receita.Descricao,
                Foto = receita.Foto,
                IdCategoria = receita.IdCategoria,
                IdReceita = receita.IdReceita,
                Ingredientes = receita.Ingredientes,
                ModoDePreparo = receita.ModoDePreparo,
                NomeDoArquivo = receita.NomeDoArquivo,
                Tags = receita.Tags,
                Titulo = receita.Titulo
            };

            return receitaVM;
        }

        // PUT: api/Receitas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReceita(int id, Receita receita)
        {
            if (id != receita.IdReceita)
            {
                return BadRequest();
            }

            _context.Entry(receita).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReceitaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Receitas
        [HttpPost]
        public async Task<ActionResult<Receita>> PostReceita(Receita receita)
        {
            _context.Receitas.Add(receita);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetReceita", new { id = receita.IdReceita }, receita);
        }

        // DELETE: api/Receitas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Receita>> DeleteReceita(int id)
        {
            var receita = await _context.Receitas.FindAsync(id);
            if (receita == null)
            {
                return NotFound();
            }

            _context.Receitas.Remove(receita);
            await _context.SaveChangesAsync();

            return receita;
        }

        private bool ReceitaExists(int id)
        {
            return _context.Receitas.Any(e => e.IdReceita == id);
        }
    }
}
