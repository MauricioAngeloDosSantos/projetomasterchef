﻿using Microsoft.EntityFrameworkCore;
using ProjetoMasterChef.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoMasterChef.WebAPI.Models
{
    public class DbMasterChefContext : DbContext
    {
        public DbMasterChefContext(DbContextOptions<DbMasterChefContext> options) : base(options) {}

        public DbSet<Receita> Receitas { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
    }
}
