﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjetoMasterChef.Applications;
using ProjetoMasterChef.Domains.Entities;
using ProjetoMasterChef.Models;
using ProjetoMasterChef.Helpers;
using ProjetoMasterChef.Domains.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace ProjetoMasterChef.Controllers
{
    public class ReceitasController : Controller
    {
        private readonly ReceitasApp AppService;
        private readonly IConfiguration configuration;
        private readonly CategoriaApp categoriaApp;

        public ReceitasController(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.AppService = new ReceitasApp(this.configuration["WebApiOptions:HostClientAddress"]);
            this.categoriaApp = new CategoriaApp(this.configuration["WebApiOptions:HostClientAddress"]);
        }

        // GET: Receitas
        public async Task<IActionResult> Index()
        {
            IEnumerable<ReceitaVM> receitas = await this.AppService.GetAll();

            return View(receitas);
        }

        // GET: Receitas/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var receita = await this.AppService.GetById((long)id);

            if (receita == null)
            {
                return NotFound();
            }

            return View(receita);
        }

        [Authorize]
        // GET: Receitas/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            ReceitaVM receita;

            if (id == null)
            {
                ViewData["Title"] = "Adiciona";
                receita = new ReceitaVM();
            }
            else
            {
                ViewData["Title"] = "Edita";

                receita = await this.AppService.GetById((long)id);

            }

            
            var categorias = await categoriaApp.GetAll();

            ViewData["IdCategoria"] = new SelectList(categorias, "IdCategoria", "Titulo", receita.IdCategoria);
            ViewData["Error"] = "";
            return View(receita);
        }

        // POST: Receitas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, ReceitaVM receita, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null)
                    {
                        MessageReturn messageReturn = FormFileHelper.IsValid(file);

                        if (messageReturn.ReturnType != MessageReturn.MessageReturnType.Ok)
                        {
                            var categorias = await categoriaApp.GetAll();

                            ViewData["Error"] = messageReturn.Message;
                            ViewData["IdCategoria"] = new SelectList(categorias, "IdCategoria", "Titulo", receita.IdCategoria);
                            return View(receita);
                        }

                        string imagemBaseString64 = FormFileHelper.FormFileToBase64String(file);

                        receita.NomeDoArquivo = file.Name;
                        receita.ContentType = file.ContentType;
                        receita.Foto = $"data:{receita.ContentType};base64,{imagemBaseString64}"; 
                    }


                    if (id == 0)
                        this.AppService.Add(receita);
                    else
                        this.AppService.Update(id, receita);

                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReceitaExists(receita.IdReceita))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdCategoria"] = new SelectList(await categoriaApp.GetAll(), "IdCategoria", "Descricao", receita.IdCategoria);
            return View(receita);
        }

        [Authorize]
        // GET: Receitas/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var receita = await this.AppService.GetById((long)id);
            if (receita == null)
            {
                return NotFound();
            }

            return View(receita);
        }

        // POST: Receitas/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            this.AppService.Delete((long)id);

            return RedirectToAction(nameof(Index));
        }

        private bool ReceitaExists(int id)
        {
            return this.AppService.GetById((long)id) != null;
        }

        [Authorize]
        [HttpPost]
        [Route("Receitas/Upload")]
        public JsonResult Upload(IFormFile file)
        {
            return Json(UpLoadHelper.ImgUpload(file));
        }
    }
}
