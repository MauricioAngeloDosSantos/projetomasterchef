﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProjetoMasterChef.Applications;
using ProjetoMasterChef.Domains.Entities;
using ProjetoMasterChef.Helpers;
using ProjetoMasterChef.Models;

namespace ProjetoMasterChef.Controllers
{
    public class CategoriasController : Controller
    {

        private readonly CategoriaApp AppService;
        private readonly IConfiguration configuration;
        
        public CategoriasController(IConfiguration configuration)
        {
            this.configuration = configuration;
            AppService = new CategoriaApp(this.configuration["WebApiOptions:HostClientAddress"]);
        }

        // GET: Categorias
        public async Task<IActionResult> Index()
        {
            return View(await this.AppService.GetAll());
        }

        // GET: Categorias/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoria = await this.AppService.GetById((long)id);
                
            if (categoria == null)
            {
                return NotFound();
            }

            return View(categoria);
        }


        [Authorize]
        // GET: Categorias/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            Categoria categoria;

            if (id == null)
            {
                ViewData["Title"] = "Adiciona";
                categoria = new Categoria();
            }
            else
            {
                ViewData["Title"] = "Edita";
                categoria = await this.AppService.GetById((long)id);
            }

            return View(categoria);
        }

        // POST: Categorias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("IdCategoria,Titulo,Descricao,Foto,ContentType,NomeDoArquivo")] Categoria categoria, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null)
                    {
                        MessageReturn messageReturn = FormFileHelper.IsValid(file);

                        if (messageReturn.ReturnType != MessageReturn.MessageReturnType.Ok)
                        {
                            ViewData["Error"] = messageReturn.Message;
                            return View();
                        }

                        string imagemBaseString64 = FormFileHelper.FormFileToBase64String(file);

                        categoria.NomeDoArquivo = file.Name;
                        categoria.ContentType = file.ContentType;
                        categoria.Foto = $"data:{categoria.ContentType};base64,{imagemBaseString64}";
                    }

                    if (id == 0)
                        this.AppService.Add(categoria);
                    else
                        this.AppService.Update(categoria.IdCategoria, categoria);

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoriaExists(categoria.IdCategoria))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(categoria);
        }

        [Authorize]
        // GET: Categorias/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoria = await this.AppService.GetById((long)id);
            if (categoria == null)
            {
                return NotFound();
            }

            return View(categoria);
        }

        // POST: Categorias/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public void DeleteConfirmed(long id)
        {
            this.AppService.Delete((long)id);
            
        }

        private bool CategoriaExists(long id)
        {
            return this.AppService.GetById(id) != null;
        }

        [Authorize]
        [HttpPost]
        [Route("Categorias/Upload")]
        public JsonResult Upload(IFormFile file)
        {
            return Json(UpLoadHelper.ImgUpload(file));
        }
    }
}
