﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProjetoMasterChef.Applications;
using ProjetoMasterChef.Domains.Entities;
using ProjetoMasterChef.Models;

namespace ProjetoMasterChef.Controllers
{
    public class HomeController : Controller
    {
        private readonly CategoriaApp AppService;
        private readonly IConfiguration configuration;

        public HomeController(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.AppService = new CategoriaApp(this.configuration["WebApiOptions:HostClientAddress"]);
        }

        public async Task<IActionResult> Index()
        {
            var categorias = await this.AppService.GetAll();

            return View(categorias.ToList().Take(3));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
