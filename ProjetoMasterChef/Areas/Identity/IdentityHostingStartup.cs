﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjetoMasterChef.Areas.Identity.Data;
using ProjetoMasterChef.Models;

[assembly: HostingStartup(typeof(ProjetoMasterChef.Areas.Identity.IdentityHostingStartup))]
namespace ProjetoMasterChef.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<DbMasterChefIdentityContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("DbMasterChefIdentityContextConnection")));

                services.AddDefaultIdentity<MasterChefUser>()
                    .AddEntityFrameworkStores<DbMasterChefIdentityContext>();
            });
        }
    }
}