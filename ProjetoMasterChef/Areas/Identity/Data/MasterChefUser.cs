﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ProjetoMasterChef.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the MasterChefUser class
    public class MasterChefUser : IdentityUser
    {
    }
}
