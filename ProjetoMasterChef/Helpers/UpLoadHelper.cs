﻿using Microsoft.AspNetCore.Http;
using ProjetoMasterChef.Domains.Entities;

namespace ProjetoMasterChef.Helpers
{
    public static class UpLoadHelper
    {
        public static object ImgUpload(IFormFile file)
        {
            if (file != null)
            {
                MessageReturn messageReturn = FormFileHelper.IsValid(file);

                if (messageReturn.ReturnType != MessageReturn.MessageReturnType.Ok)
                {
                    return new { sucesso = false, erro = messageReturn.Message, imagem = "" };
                }

                string imagemBaseString64 = FormFileHelper.FormFileToBase64String(file);

                return new { sucesso = true, erro = "", imagem = $"data:{file.ContentType};base64,{imagemBaseString64}" };
            }
            else
            {
                return new { sucesso = false, erro = "Não foi encontrado nenhum arquivo para carregar.", imagem = "" };
            }

        }
    }
}
