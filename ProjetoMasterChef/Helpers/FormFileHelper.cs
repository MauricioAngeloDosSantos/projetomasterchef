﻿using Microsoft.AspNetCore.Http;
using ProjetoMasterChef.Domains.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoMasterChef.Helpers
{
    public static class FormFileHelper
    {
        public static string FormFileToBase64String(IFormFile file)
        {
            byte[] bytesImg;

            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                bytesImg = ms.ToArray();
            }

            return Convert.ToBase64String(bytesImg);
        }

        public static MessageReturn IsValid(IFormFile file)
        {
            string tipoDeArquivos = "image/jpg; image/jpeg; image/png; image/bmp";

            // Verifica se o arquivo é de imagem
            if (!tipoDeArquivos.Contains(file.ContentType.ToLower()))
            {
                return MessageReturn.Erro( $"Apenas arquivos do tipo '{tipoDeArquivos}', são permitidos. O arquivo que você selecionou é do tipo '{file.ContentType.ToLower()}'");
            }

            // Verifica se o arquivo é muito grande
            if (file.Length > 65000)
            {
                return MessageReturn.Erro($"Arquivo '{file.Name}', é muito grande. O sistema permite apenas arquivos com no máximo 65000kb");
            }

            return MessageReturn.Ok();
        }
    }
}
